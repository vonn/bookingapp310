import smtplib


class Emailer():
    def __init__(self):
        pass

    @staticmethod
    def send_meeting_email(user, meeting_host, meeting):
        subject = "New meeting!"
        body = "Greetings " + user.name + ",\n\n" + \
               meeting_host + " has added you to his event.\n\n" + \
               "GUESTS: " + str(meeting.guests) + "\n\n" \
                                                  "DATE: " + meeting.date.strftime("%B %d, %Y") + "\n\n" \
                                                                                                  "TIME: " + str(
            meeting.slot)

        message = "Subject: %s\n\n%s" % (subject, body)

        Emailer.send_mail(user.email, message)

    @staticmethod
    def send_mail(email, message):
        mail = smtplib.SMTP("smtp.gmail.com", 587)
        mail.starttls()
        mail.login("kdramos@cs.stonybrook.edu", "b0gusPassword")
        mail.sendmail("kdramos@cs.stonybrook.edu", email, message)
