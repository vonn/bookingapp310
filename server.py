# pickle socket test server

import cPickle as pickle
import socket as sock

import request
import user as user
import availability as avail
import emailer
import auth


host = 'localhost'
port = 10010
buffer_size = 1024
backlog = 5

try:
    s = sock.socket(sock.AF_INET, sock.SOCK_STREAM)
    s.bind((host, port))
    s.listen(backlog)
except sock.error as se:
    print "[Error] " + str(se)

users = {}


def write(_socket, data):
    f = _socket.makefile('wb', buffer_size)
    pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
    f.close()


def read(_socket):
    f = _socket.makefile('rb', buffer_size)
    data = pickle.load(f)
    f.close()
    return data


# handle a request data from a client
def handle_req(c_req, client, addr):
    print "Request from " + addr[0] + ": " + c_req.request
    # CLIENT REQUESTS THE USERS LIST
    if c_req.request == "REQUEST_USERS":
        # make a response for them, saying it's OK and sending them the users list
        r = request.Response(request.Response.OK, users)
        print "Users sent to " + addr[0]

    # CLIENT WANTS TO KNOW IF THIS LOGIN IS RIGHT
    elif "LOGIN|" in c_req.request:
        # split name from the request
        name = c_req.request.split('|')[1]
        # get corresponding user
        u = users[name]
        # does this match the users pw?
        match = u.password.match(c_req.data)
        if match:
            r = request.Response(request.Response.OK, "Login Successful")
        else:
            r = request.Response(request.Response.ERROR, "Incorrect password")

    # CLIENT WISHES TO SEND ME A NEW AVAILABILITY WOW
    elif "NEW_AVAIL|" in c_req.request:
        # split name from the request
        name = c_req.request.split('|')[1]
        av = c_req.data

        try:
            users[name].addavailability(av)
            # Save the USERS data file
            save_users_data()
            # and send a response back to the client, and the user's new availabilities
            r = request.Response(request.Response.OK, users[name].availabilities)
        except Exception as ex:
            r = request.Response(request.Response.ERROR, "Error adding timeslot")

    # CLIENT WISHES TO DELETE AVAILABILITY
    elif "DEL_AVAIL|" in c_req.request:
        # split name from the request
        name = c_req.request.split('|')[1]
        av = c_req.data

        try:
            users[name].removeavailability(av)
            # Save the USERS data file
            save_users_data()
            # and send a response back to the client, and the user's new availabilities
            r = request.Response(request.Response.OK, users[name].availabilities)
        except Exception as ex:
            r = request.Response(request.Response.ERROR, "Error removing timeslot (" + str(ex) + ")")

    # CLIENT WISHES TO SCHEDULE A MEETING
    elif "SCHEDULE_MEETING|" in c_req.request:
        # grab the meeting from the request
        m = c_req.data
        # and who planned it
        planner = c_req.request.split('|')[1]

        # for each guest in that meeting
        for guest_name in m.guests:
            # get the corresponding user
            guest_user = users[guest_name]
            # make an availability (because the user obj already knows how to remove that)
            av = avail.Availability(m.date)
            av.addslot(m.slot)
            # and have the user remove it
            guest_user.removeavailability(av)

        # save !
        save_users_data()
        # send the meeting info the guests
        send_new_meeting_email(m, planner)

        # all is well now, let's send a response
        r = request.Response(request.Response.OK, "Meeting planned successfully")

    # Wtf do you want?
    else:
        r = request.Response(request.Response.ERROR, "Error: Unknown request, " + c_req.request)

    write(client, r)


def send_new_meeting_email(meeting, planner):
    # TODO send the guests an email
    for g in meeting.guests:
        emailer.Emailer.send_meeting_email(users[g], planner, meeting)


def load_users_from_file():
    global users

    instasave = False

    # read the existing binary
    try:
        with open('users_data.dat', 'rb') as f:
            users = pickle.load(f)
    except ImportError as ie:
        print ie
        pass
    except IOError as ioe:
        print "User data file 'users_data.dat' does not exist. Creating..."
        instasave = True
        pass

    # open file as f
    with open('users.dat', 'r') as f:
        for line in f:
            # get user info
            name, email, hashedpw, salt = line.split(' ', 3)

            pw = auth.Password(hashedpw, salt.strip())
            # user isn't known
            if name not in users:
                u = user.User(name, email.strip(), pw)
                # append to users list
                users[u.name] = u
            # update their email or something if those've changed
            else:
                users[name].email = email
                users[name].password = pw

        # close file when we're done
        f.close()

    if instasave:
        save_users_data()

    print "Users data loaded."


def save_users_data():
    # open file as f
    with open('users_data.dat', 'wb') as f:
        pickle.dump(users, f, pickle.HIGHEST_PROTOCOL)

        # close file when we're done
        f.close()

    print "Users data saved."


def init_server():
    print "Server is listening..."
    # listening loop
    while 1:
        # accept a client
        client, address = s.accept()
        if client:
            print "Client Connected"
            # get data from the client
            client_request = read(client)
            # we have data
            if client_request and isinstance(client_request, request.Request):
                handle_req(client_request, client, address)

            client.close()
            print "Client disconnected"


def main():
    load_users_from_file()
    init_server()


if __name__ == "__main__":
    main()