'''
-- Request --
Defines a request that can be sent to the server
Request types:
    GET: to retrieve data from that server
        ex. (GET, "REQUEST_USERS") lets the server know that the client wants the users
            (GET, "AVAILABILITY", "Ken") is a request for Ken's availability
    SEND: upload data to server
        ex. (SEND, "NEW_AVAIL", newAvailability) sends a new availability to the server

-- Response --
Defines a response that is sent to the client
    ex. resp = Response("OK", users) # response to REQUEST_USERS. Sends confirmation and users obj

'''


class Request():
    GET = "GET"
    SEND = "SEND"

    def __init__(self, rtype, request, data=None):
        self.rtype = rtype
        self.request = request
        self.data = data


class Response():
    OK = "OK"
    ERROR = "ERROR"

    def __init__(self, response, data):
        self.response = response
        self.data = data