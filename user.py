import datetime as dt

import availability as avail


class User():
    def __init__(self, name, email, password):
        self.name = name
        self.email = email.rstrip()
        self.availabilities = {}
        self.password = password

    def __str__(self):
        return self.name + " (" + self.email + ")"

    def addavailability(self, av):
        # check if this date is already in availabilities
        if av.date in self.availabilities:
            # just add the new slots
            for ts in av.timeslots:
                self.availabilities[av.date].addslot(ts)
        # no date here? just tack it on, then.
        else:
            self.availabilities[av.date] = av

    def removeavailability(self, av):
        # If i don't have an availability for this av's date
        if av.date not in self.availabilities:
            # make an all-day availability for here, then
            new_av = avail.Availability(av.date)
            all_day_slot = avail.TimeSlot(dt.time(0, 00), dt.time(23, 59))  # day is from 0:00 to 23:59
            new_av.addslot(all_day_slot)
            # add it to my availabilities
            self.availabilities[av.date] = new_av

        # just remove the slots
        for ts in av.timeslots:
            self.availabilities[av.date].removeslot(ts)

    def print_availability(self):
        # start printing from today for 2 weeks
        d = dt.date.today()
        for i in range(0, 15):
            # do i have a date for today
            if d in self.availabilities:
                print self.availabilities[d]
            else:
                # make a new blank avail and print it
                av = avail.Availability(d)
                print str(av) + "All day"

            # add a day
            d += dt.timedelta(days=1)
