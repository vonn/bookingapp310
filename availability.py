class Availability():
    """ A day that a user is available """

    def __init__(self, date):
        # the date of this availability
        self.date = date
        # a list of TimeSlot objects
        self.timeslots = []

    def addslot(self, timeslot):
        did_a_thing = False

        for ts in self.timeslots:
            # DO I ENCOMPASS AND ECLIPSE THIS?
            if timeslot.start < ts.start and timeslot.end > ts.end:
                did_a_thing = True
                # just replace this fker
                ts.start = timeslot.start
                ts.end = timeslot.end

            # AM I WITHIN?
            elif timeslot.end <= ts.end and timeslot.start >= ts.start:
                did_a_thing = True
                pass

            # OVERLAP - END
            # DO I START BEFORE THIS ENDS? AND DO I END AFTER IT? AND START AFTER IT?
            elif ts.start <= timeslot.start <= ts.end < timeslot.end:
                did_a_thing = True
                # extend the time of the first, simply
                ts.end = timeslot.end

            # OVERLAP - START
            # DO I END AFTER THIS STARTS? AND DO I START BEFORE THIS DOES?
            elif timeslot.start < ts.start <= timeslot.end <= ts.end:
                did_a_thing = True
                # change this one's start time to the earlier
                ts.start = timeslot.start

        # must've been no timeslots~
        if not did_a_thing:
            self.timeslots.append(timeslot)

        # then sort the slots by start time
        self.timeslots = sorted(self.timeslots, key=lambda slot: slot.start)

    def removeslot(self, timeslot):
        for ts in self.timeslots:
            # do i encompass? and eclipse?
            if timeslot.start <= ts.start and timeslot.end >= ts.end:
                # remove whole slot
                self.timeslots.remove(ts)

            # OVERLAP - INSIDE
            elif timeslot.start > ts.start and timeslot.end < ts.end:
                # split into 2 new slots
                ts1 = TimeSlot(ts.start, timeslot.start)
                ts2 = TimeSlot(timeslot.end, ts.end)
                # remove original slot
                self.timeslots.remove(ts)
                # add the 2 new ones
                self.addslot(ts1)
                self.addslot(ts2)

            # OVERLAP - BEFORE
            elif timeslot.start <= ts.start < timeslot.end < ts.end:
                # cut the head of ts
                ts.start = timeslot.end

            # OVERLAP - AFTER
            elif ts.start < timeslot.start < ts.end <= timeslot.end:
                # cut ts tail
                ts.end = timeslot.start

    def __str__(self):
        s = str(self.date) + ": "
        for i in range(0, len(self.timeslots)):
            s += "(" + str(self.timeslots[i]) + ")"
            if i < len(self.timeslots) - 1:
                s += ", "

        return s


class TimeSlot():
    def __init__(self, start, end):
        assert start < end
        self.start = start
        self.end = end

    def __str__(self):
        return str(self.start) + " to " + str(self.end)


class Meeting():
    def __init__(self, date, slot, guests):
        self.date = date
        self.slot = slot
        self.guests = guests