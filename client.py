# pickle socket test client

import cPickle as pickle
import socket as sock
import datetime as dt
import re
import sys
# my files
import request as req
import availability as avail

argv = sys.argv
try:
    host = argv[1]
    port = int(argv[2])
# Try these defaults, if no arguments are given
except:
    host = "localhost"
    port = 10010
buffer_size = 1024
# The User object associated with the end user
me = None
# List of User objects (retrieved from server)
users = {}


def write(_socket, data):
    f = _socket.makefile('wb', buffer_size)
    pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
    f.close()


def read(_socket):
    f = _socket.makefile('rb', buffer_size)
    data = pickle.load(f)
    f.close()
    return data


def send_request(request):
    server = sock.socket(sock.AF_INET, sock.SOCK_STREAM)
    try:
        # connect to server
        server.connect((host, port))
        # send the request to server
        write(server, request)
        # get the server's response
        response = read(server)
        server.close()
        # print "Server response: " + str(response)
        return response
    except sock.timeout:
        print "[Error] Connection timed out"
    except sock.error as se:
        print "[Error] " + str(se)
        if se.errno == -5 or 111:
            print "[Error] Invalid Server Address. Please check the host and port."
            sys.exit()
    finally:
        pass


def get_user_by_name(name):
    for u in users:
        if u.name == name:
            return u
    return None


def get_time():
    parts = None
    # HOW LONG IS THIS MEETING
    timeregex = re.compile("\d?\d:[03]0")
    while not parts or len(parts) != 2:
        inp = raw_input("Enter duration of meeting (HH:MM): ")
        if timeregex.match(inp):
            parts = inp.split(':')
            try:
                t = dt.time(int(parts[0]), int(parts[1]))
                # don't let 0:00 through
                if t.hour is 0:
                    assert t.min is not 0
            except AssertionError:
                print "Invalid duration: Must be at least 30min long"
                parts = None
            except ValueError:
                print "Invalid time: Hours are 0-23"
                parts = None  # set this to None so it loops
        else:
            print "Invalid format, use HH:MM, with MM as either 00 or 30."
    return t


def get_guestlist():
    bonk = False
    guestlist = []
    while not bonk:
        who = raw_input("Enter name of guest #" + str((len(guestlist) + 1)) + " (enter blank when done): ").strip()

        if len(who) == 0:
            bonk = True
            break

        if who not in users:
            print who + " is not a valid user"
        elif who in guestlist:
            print who + " is already on the list"
        else:
            print who + " added to guest list"
            guestlist.append(who)
    return guestlist


def get_date():
    parts = None
    # GET DATE
    dateregex = re.compile(
        "\d{4}-([0]?[1-9]|1[0-2])-(3[01](?!3)|[012]?[1-9])")  # i made this regex myself pls notice me
    while not parts or len(parts) != 3:
        inp = raw_input("Date (YYYY-MM-DD): ")
        inp.strip()
        if dateregex.match(inp):
            parts = inp.split('-')
            yr = int(parts[0])
            mo = int(parts[1])
            day = int(parts[2])
            d = dt.date(yr, mo, day)
            if d < dt.date.today():
                print "Error: Date cannot be before today"
                parts = None
        else:
            print "Invalid format, use YYYY-MM-DD"

    return d


def get_timeslot():
    parts = None
    # GET TIME SLOT
    slotregex = re.compile("\d?\d:[03]0-\d?\d:[03]0")
    while not parts or len(parts) != 2:
        inp = raw_input("Time slot (HH:MM-HH:MM): ")
        if slotregex.match(inp):
            parts = inp.split('-')
            time1 = parts[0]
            t1parts = time1.split(':')
            time2 = parts[1]
            t2parts = time2.split(':')
            # Make a TimeSlot of the input
            try:
                ts = avail.TimeSlot(
                    dt.time(int(t1parts[0]), int(t1parts[1])),
                    dt.time(int(t2parts[0]), int(t2parts[1]))
                )
            except AssertionError:
                print "Invalid time slot: The end time has to be after start time"
                parts = None  # set this to None so it loops
            except ValueError:
                print "Invalid time slot: Hours are 0-23"
                parts = None  # set this to None so it loops
        else:
            print "Invalid format, use HH:MM-HH:MM, with MM as either 00 or 30."

    return ts


def get_intersecting_slots(guestlist, date, time_range, duration):
    failcase = False

    # check each guest
    for g in guestlist:
        available = False
        g = users[g]

        # if i don't have any availabilities for this date
        if date not in g.availabilities:
            available = True
            # get all the valid slots in this range
            valid_slots = get_all_slots_in_range(date, time_range, duration)

        else:
            valid_slots = []
            for ts in g.availabilities[date].timeslots:
                slots = get_all_slots_in_range(date, ts, duration)
                valid_slots.extend(slots)

            # Do i have slots?
            if len(valid_slots) > 0:
                available = True

        # if i'm not available, this should fail
        if not available:
            failcase = True
            break

    # There was a conflict (user isn't available), so no valid slots should be returned
    if failcase:
        return None
    else:
        return valid_slots


def get_all_slots_in_range(date, time_range, duration):
    slots = []
    curr_time = dt.datetime.combine(date, time_range.start)
    # last time is the end of the timerange - duration
    last_time = dt.datetime.combine(date, time_range.end) \
                - dt.timedelta(hours=duration.hour, minutes=duration.minute, seconds=duration.second)

    if last_time < curr_time:
        return slots

    while True:
        t1 = curr_time.time()
        t2 = (curr_time + dt.timedelta(hours=duration.hour, minutes=duration.minute, seconds=duration.second)).time()

        try:
            # grab a slot here
            slot = avail.TimeSlot(t1, t2)
        except AssertionError as ae:
            print "Something is goof"
            break

        # add it to the list
        slots.append(slot)

        # break out if this was the last time
        if curr_time >= last_time:
            break;
        # add 30 min to check next step
        else:
            curr_time += dt.timedelta(minutes=30)

    return slots


# displays main menu and handles user's choice
def menu_main():
    # log in TODO bonus task: actual user/pw login from server
    def menu_login():
        global me
        logged_in = False

        while not logged_in:
            # Get a username
            name = raw_input("Log in name: ")
            # check all my users for this
            if name in users:
                me = users[name]
            else:
                print "There is no user named " + name

            # Did i enter valid username
            if me is None:
                menu_login()
            else:
                pw = raw_input("Enter Password: ")
                # make req
                r = req.Request(req.Request.GET, "LOGIN|" + me.name, pw)
                resp = send_request(r)
                # successful login
                if resp.response == req.Response.ERROR:
                    print "[Error] " + resp.data
                else:
                    logged_in = True

    if me is None:
        menu_login()
        print "Logged in as " + me.name

    # MY AVAILABILITY
    def menu_my_availability():
        # Ask the user to add an availability
        def menu_get_avail(todo):
            """
            Gets an availability date and slot from the user
            :param todo: what to do after the user enters a date and timeslot (ADD/REMOVE)
            """
            d = get_date()
            ts = get_timeslot()

            # IF I WANNA ADD/EDIT AVAILABILITY
            if todo == "ADD/EDIT":
                # get this availability if it's there
                if d in me.availabilities:
                    av = me.availabilities[d]
                # otherwise, make new Availability object
                else:
                    av = avail.Availability(d)
                av.addslot(ts)

                # form a request, to let the server know we're sending it an availability for me
                r = req.Request(req.Request.SEND, "NEW_AVAIL|" + me.name, av)

                resp = send_request(r)

                # if the request was successful,
                if resp.response == req.Response.OK:
                    print "Availability successfully uploaded"
                    # i can add this to my local avail list
                    me.availabilities = resp.data
                elif resp.response == req.Response.ERROR:
                    # else, let user know there was a scheduling conflicterino, or something goofed
                    print "Error: " + resp.data

            # IF I WANNA DELETE AVAILABILITY
            elif todo == "DELETE":
                """if d not in me.availabilities:
                    print "You have no nothing to delete on this day: " + str(d)
                    return"""

                av = avail.Availability(d)
                av.addslot(ts)
                # form a request, saying i wanna delete an availability
                r = req.Request(req.Request.SEND, "DEL_AVAIL|" + me.name, av)

                resp = send_request(r)

                # if the request was successful,
                if resp.response == req.Response.OK:
                    print "Availability successfully deleted"
                    me.availabilities = resp.data
                elif resp.response == req.Response.ERROR:
                    print "Error: " + resp.data

            else:
                raise Exception("IDK WHAT TO DO (???????)")

        choices_my_avail = ['1', '2', '3', '4']

        # display menu choices
        print "~~~~~\tMy Availability\t~~~~~"
        me.print_availability()
        print "~~~~~~~~~~~~~~~~~~~~~~~~~"
        print "1\t~\tEnter/Modify availability"
        print "2\t~\tDelete availability"
        print "3\t~\tCancel"
        print "~~~~~~~~~~~~~~~~~~~~~~~~~"

        # get option
        c = None
        while c not in choices_my_avail:
            c = raw_input("Select an option (1-3): ")

        # Add/edit a new availability
        if c == '1':
            menu_get_avail("ADD/EDIT")
        if c == '2':
            menu_get_avail("DELETE")
            # '3' will fall through and go back to main menu

    def menu_other_availability():
        # Print list of users
        print "~~~~~\tUser List\t~~~~~"
        # refresh users list first
        net_get_users()
        i = 0
        for name, u in users.iteritems():
            print str(i + 1) + "\t~\t" + str(users[name])
            i += 1
        print "~~~~~~~~~~~~~~~~~~~~~~~~~"
        # Ask user who they'd like to check
        u = None
        while u is None:
            who = raw_input("Whose availability would you like to check? (leave blank to go back): ")

            if len(who) == 0:
                return

            try:
                # If they entered a number, get that user (via their listed number)
                if int(who) in range(1, len(users) + 1):
                    # must be trying to look by number
                    u = users.values()[int(who) - 1]
            except ValueError:
                # must've typed their name
                u = users[who]

            print "~~~~\t" + u.name + "'s Availability\t~~~~"
            u.print_availability()
            print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

    def menu_plan_meeting():
        # GET LIST OF GUESTS FROM USER
        guestlist = get_guestlist()

        # GET DATE
        d = get_date()

        # GET DURATION
        t = get_time()

        # WHAT TIME RANGE SHOULD I LOOK IN?
        ts = get_timeslot()

        # find the intersecting slots of these users, on this day, of this duration, in this time range
        magic_slots = get_intersecting_slots(guestlist, d, ts, t)

        if magic_slots is not None:
            print "~~~\tAvailable Slots\t~~~"
            for i in range(0, len(magic_slots)):
                print str(i + 1) + "\t~\t" + str(magic_slots[i])
            print "~~~\t~~~~~~~~~~~~~~~\t~~~"
            num = None
            while num is None:
                # Ask user for which slot they want
                num = raw_input("Pick a slot (1-" + str(len(magic_slots)) + "): ").strip()
                try:
                    # if they entered something and it's a number within the range
                    if len(num) > 0 and (1 <= int(num) <= len(magic_slots) + 1):
                        # grab this slot
                        s = magic_slots[int(num) - 1]
                        m = avail.Meeting(d, s, guestlist)
                        # make a request for the server
                        r = req.Request(req.Request.SEND, "SCHEDULE_MEETING|" + me.name, m)
                        # and send that bad boy
                        resp = send_request(r)
                        if resp.response == req.Response.OK:
                            print "Meeting successfully planned."
                            # refresh the users from server
                            net_get_users()
                        else:
                            print "[Error] " + resp.data
                    # invalid number
                    else:
                        print "Number must be in range (1-" + str(len(magic_slots)) + ")"
                        num = None
                # Not a number or something
                except ValueError:
                    print "Invalid input"
                    num = None
        else:
            print "[ERROR] No availabilities matching everyone for this meeting"


    menu_quit = False
    choices_main = ['1', '2', '3', '4']
    # display menu choices
    print "~~~~~\tMain Menu\t~~~~~"
    print "1\t~\tDisplay my availability"
    print "2\t~\tDisplay another user's availability"
    print "3\t~\tPlan meeting"
    print "4\t~\tQuit"
    print "~~~~~~~~~~~~~~~~~~~~~~~~~"
    choice = -1
    # get menu choice
    while choice not in choices_main:
        choice = raw_input("~ Enter choice (1-4): ")

    # ~~ handle menu choice ~~
    # my availability
    if choice == '1':
        menu_my_availability()
    # others' availability
    if choice == '2':
        menu_other_availability()
    # plan meeting
    if choice == '3':
        menu_plan_meeting()
    # quit
    if choice == '4':
        menu_quit = True

    if not menu_quit:
        raw_input("Press [Enter] to continue...")
        menu_main()
    else:
        print "Goodbye!"


def net_get_users():
    # form request
    r = req.Request(req.Request.GET, "REQUEST_USERS")
    resp = send_request(r)
    for name, u in resp.data.iteritems():
        users[name] = u


# start the application
def start():
    print "Hello! Starting Availability client.."
    # retrieve users and data from server
    print "Retrieving data from server..."
    net_get_users()
    print "Success, starting application...\r\n"
    menu_main()


if __name__ == "__main__":
    start()