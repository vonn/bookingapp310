import uuid
import hashlib


class Auth():
    def __init__(self):
        pass

    @staticmethod
    def gen_salt():
        return uuid.uuid4().hex

    @staticmethod
    def hash(pw, salt):
        return hashlib.sha256(salt + pw).hexdigest()


class Password():
    # Makes a password from premade salt and hashedpw
    def __init__(self, hashedpw, salt):
        self._hashedpw = hashedpw
        self._salt = salt

    @staticmethod
    # Generates hashedpw and salt for given pw string
    def gen_pw(password):
        salt = Auth.gen_salt()
        hashedpw = Auth.hash(password, salt)
        return Password(hashedpw, salt)

    def match(self, test):
        return Auth.hash(test, self._salt) == self._hashedpw